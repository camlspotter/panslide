#!/bin/sh

# Requirements
#
# * Pandoc to compile *.md to html
# * reveal.js
# * Chrome
# * chrome-cli
# * fswatch
# * decktape, if you want to make a slide PDF
#        decktape --size='2048x1536' aaa.html aaa.pdf


# Options:  .panslide file
#
# Examplle of .panslide file
#
# # Link these files and directories under _build/
# LINK="../reveal.js ../images"
#
# # Extra files and directories to monitor other than *.md and *.css
# WATCH=../images

function force_exit () {
    exit 1
}

trap force_exit SIGINT

if [ ! -d _build ]; then
    mkdir _build
fi

if [ -f .panslide ]; then
    source .panslide
fi

for i in $LINK
do
    f=`pwd`/$i
    echo LINK $f
    rm -f _build/`basename $i`
    ln -s $f _build/`basename $i`
done

while true; do

    tabs=`chrome-cli list links | grep 'slides/.*/_build/.*html' | sed  -e 's/^\[\([0-9:]*\)\].*/\1/' -e 's/.*://'`
    echo tabs $tabs
    for f in *.md
    do
	i=`echo $f | sed -e 's/\.md//'`
	pandoc -s -t revealjs -o _build/$i.html $i.md \
	       --mathjax \
	       --slide-level=2 \
	       -V theme:serif \
	       --metadata pagetitle="$i" # -V showNotes:true

	if [ -z $tabs ]; then
	    echo LOAD file://`pwd`/_build/$i.html
	    chrome-cli open file://`pwd`/_build/$i.html
	fi
    done

    for i in $tabs
    do
	echo RELOAD tab $i
	chrome-cli reload -t $i
    done

    fswatch -1 *.md *.css $WATCH
done
